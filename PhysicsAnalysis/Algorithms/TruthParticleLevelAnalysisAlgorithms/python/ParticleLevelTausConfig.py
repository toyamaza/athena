# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock


class ParticleLevelTausBlock(ConfigBlock):
    """ConfigBlock for particle-level truth taus"""

    def __init__(self):
        super(ParticleLevelTausBlock, self).__init__()
        self.addOption('containerName', 'TruthTaus', type=str,
                       info='the name of the input truth taus container')
        self.addOption('selectionName', '', type=str,
                       info='the name of the selection to create. The default is "",'
                       ' which applies the selection to all truth taus.')
        self.addOption('isolated', True, type=bool,
                       info='select only truth taus that are isolated.')
        # Always skip on data
        self.setOptionValue('skipOnData', True)

    def makeAlgs(self, config):
        config.setSourceName (self.containerName, self.containerName)

        # decorate the missing elements of the 4-vector so we can save it later
        alg = config.createAlgorithm('CP::ParticleLevelPtEtaPhiDecoratorAlg',
                                     'ParticleLevelPtEtaPhiDecoratorTaus' + self.selectionName,
                                     reentrant=True)
        alg.particles = self.containerName

        # decorate the charge so we can save it later
        alg = config.createAlgorithm('CP::ParticleLevelChargeDecoratorAlg',
                                     'ParticleLevelChargeDecoratorTaus' + self.selectionName,
                                     reentrant=True)
        alg.particles = self.containerName

        # check for prompt isolation and possible origin from tau decays
        alg = config.createAlgorithm('CP::ParticleLevelIsolationAlg',
                                     'ParticleLevelIsolationTaus' + self.selectionName,
                                     reentrant=True)
        alg.particles    = self.containerName
        alg.isolation    = 'isIsolated' + self.selectionName if self.isolated else 'isIsolatedButNotRequired' + self.selectionName
        alg.notTauOrigin = 'notFromTauButNotRequired' + self.selectionName
        alg.checkType    = 'IsoTau'

        if self.isolated:
            config.addSelection (self.containerName, self.selectionName, alg.isolation+',as_char')

        # output branches to be scheduled only once
        if ParticleLevelTausBlock.get_instance_count() == 1:
            outputVars = [
                ['pt', 'pt'],
                ['eta', 'eta'],
                ['phi', 'phi'],
                ['e', 'e'],
                ['charge', 'charge'],
                ['IsHadronicTau', 'IsHadronicTau'],
                ['classifierParticleType', 'type'],
                ['classifierParticleOrigin', 'origin'],
            ]
            for decoration, branch in outputVars:
                config.addOutputVar (self.containerName, decoration, branch, noSys=True)
